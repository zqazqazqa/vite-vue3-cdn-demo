import {defineConfig} from 'vite'
import vue from '@vitejs/plugin-vue'
import importToCDN, {autoComplete} from 'vite-plugin-cdn-import'
import viteCompression from 'vite-plugin-compression';
import visualizer from "rollup-plugin-visualizer";

// https://vitejs.dev/config/
export default defineConfig({
    base:'./',
    plugins: [vue(), importToCDN({
        modules: [
            {
                name: 'moment',
                var: 'moment',
                path: 'https://unpkg.com/moment@2.29.3/moment.js'
            },
            {
                name: 'vue',
                var: 'Vue',
                path: 'https://unpkg.com/vue@3.2.25/dist/vue.global.prod.js'
            },
            {
                name: 'ant-design-vue',
                var: 'antd',
                path: [
                    'https://unpkg.com/ant-design-vue@2.2.8/dist/antd.min.js',
                ],
                css: [
                    'https://unpkg.com/ant-design-vue@2.2.8/dist/antd.min.css',
                ],
            },
            {
                name: 'vuex',
                var: 'Vuex',
                path: 'https://unpkg.com/vuex@4.0.2/dist/vuex.global.prod.js'
            },
            {
                name: 'vue-router',
                var: 'VueRouter',
                path: 'https://unpkg.com/vue-router@4.0.12/dist/vue-router.global.prod.js'
            },
        ]
    }),
        viteCompression({
            verbose: true,
            disable: false,
            algorithm: 'gzip',
            ext: '.gz'
        }),
        viteCompression({
            verbose: true,
            disable: false,
            algorithm: 'brotliCompress',
            ext: '.br'
        }),
        visualizer({
            open: true,
            gzipSize: true,
            brotliSize: true,
        })]
})
