import {createWebHashHistory, createRouter, RouteLocationNormalized, NavigationGuardNext} from "vue-router"
import a1 from "../view/a1/a1.vue";
import a2 from "../view/a2/a2.vue";

const router = createRouter({
    history: createWebHashHistory(),
    routes: [
        {
            path:'/1',
            component: a1
        },
        {
            path:'/2',
            component: a2
        },
        {
            path:'/',
            redirect:'/1'
        },
    ]
})


export default router;

